<?php
    //---------------1.1---------------
    echo "<h3>Task 1.1</h3>";

    $a = 152;
    $b = '152';
    $c = 'London';
    $d = array(152);
    $e = 15.2;
    $f = false;
    $g = true;

    echo "a is " . gettype($a) . "<br/>".
         "b is " . gettype($b) . "<br/>".
         "c is " . gettype($c) . "<br/>".
         "d is " . gettype($d) . "<br/>".
         "e is " . gettype($e) . "<br/>".
         "f is " . gettype($f) . "<br/>".
         "g is " . gettype($g) . "<br/>";


    //---------------1.2---------------
    echo "<h3>Task 1.2</h3>";

    $a = 5;
    $b = 10;

    echo "1)$a из {$b}ти баллов заслуживает фильм 'Отряд Самоубийц'<br>";
    echo '2)' . $a . ' из ' . $b . 'ти баллов заслуживает фильм \'Отряд Самоубийц\'';


    //---------------1.3---------------
    echo "<h3>Task 1.3</h3>";

    $good = 'Доброе утро';
    $ledies = 'дамы';
    $gentelmens = 'и господа';

    echo $good . "<br/>" .
         $ledies . "<br/>" .
         $gentelmens . "<br/>";

    $result .= $good;
    $result .= ', ';
    $result .= $ledies;
    $result .= ' ';
    $result .= $gentelmens;
    echo $result;


    //---------------1.4---------------
    echo "<h3>Task 1.4</h3>";

    $array1 = [1,2,3,4,5];
    $array2 = ['мама', 'мыла', 'раму', 'вчера', 'утром'];

    $array1['element'] = 10;
    unset($array2[0]);

    echo $array1[2] . "<br/>".
         $array2[2] . "<br/>";

    echo "<pre>";
    var_dump($array1);
    var_dump($array2);
    echo "</pre>";

    echo count($array1) . "<br/>";
    echo count($array2);


    //---------------2.1---------------
    echo "<h3>Task 2.1</h3>";

    define('MIN', 10);
    define('MAX', 50);
    $x = rand(0, 60);

    echo "x = $x <br/>";

    if ($x > MIN && $x < MAX){
        echo "+";
    }else if ($x == MIN || $x == MAX){
        echo "+-";
    }else{
        echo "-";
    }


    //---------------2.2---------------
    echo "<h3>Task 2.2</h3>";

    $a = rand(1, 5);
    $b = rand(1, 10);
    $c = rand(1, 5);
    echo "a = $a, b = $b, c = $c <br/>";

    $D = pow($b,2) - 4 * $a * $c;
    echo "D = $D<br/>";

    if ($D > 0){
        $x1 = ($b * (-1) + sqrt($D)) / (2 * $a);
        $x2 = ($b * (-1) - sqrt($D)) / (2 * $a);
        echo "x1 = " . round($x1, 2) . "<br/>x2 = " . round($x2, 2);
    }else if ($D == 0){
        $x = ($b * (-1)) / 2 * $a;
        echo "x = $x";
    }else{
        echo "Нет корней";
    }


    //---------------2.2---------------
    echo "<h3>Task 2.3</h3>";

    $a = rand(1, 10);
    $b = rand(1, 10);
    $c = rand(1, 10);
    echo "a = $a, b = $b, c = $c <br/>";

    if($a == $b || $a == $c || $c == $b){
        echo "Ошибка";
    }else if (($c > $b && $b > $a) || ($c < $b && $b < $a)){
        echo "Среднее число: b = $b";
    }else if (($c > $a && $a > $b) || ($c < $a && $a < $b)){
        echo "Среднее число: a = $a";
    }else {
        echo "Среднее число: c = $c";
    }


    //---------------3.1---------------
    echo "<h3>Task 3.1</h3>";

    for ($i = 1, $sum = 0; $i <= 25; $i++){
        $sum += $i;
    }
    echo "1)Сумма = $sum<br/>";

    $sum = 0;
    $i = 0;
    while ($i <=25){
        $sum += $i;
        $i++;
    }
    echo "2)Сумма = $sum";


    //---------------3.2---------------
    echo "<h3>Task 3.2</h3>";

    $N = rand(1,1000);
    echo "N = $N<br>";

    for($i = 0; $pow = pow($i, 2), $pow < $N; $i++){
        echo "$pow, ";
    }


    //---------------3.3---------------
    echo "<h3>Task 3.3</h3>";

    for($i = 10; $i >= 1; $i--){
        $array3[$i] = 'Кнопка ' . $i;
    }

    natsort($array3);

    echo "<ul>";
    foreach ($array3 as $value){
        echo '<li><a href="#">' . $value . '</a></li>';
    }
    echo "</ul>";
?>