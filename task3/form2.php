<!DOCTYPE html>
<html>
<head>
    <title>task 3</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div>
    <form action="/task3/form2.php" method="POST">

        <input type="text" name="name" value="" /> <br/>
        <input type="radio" name="gender" value="male" checked/> Мужской<br/>
        <input type="radio" name="gender" value="female" /> Женский<br/>

        <input type="submit" name="submit" value="submit" />
    </form>
</div>
</body>
</html>

<?php
    if (isset($_POST['submit'])) {
        $name = $_POST['name'];

        if ($_POST['gender'] == 'male') {
            echo "Добро пожаловать, мистер " . $name . "!";
        } else {
            echo "Добро пожаловать, миссис " . $name . "!";
        }
    }
?>