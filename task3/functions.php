<?php
    //---------------1.1---------------
    echo "<h3>Task 1.1</h3>";

	$products = array(    
		array('name' => 'Телевизор', 'price' => '400', 'quantity' => 1),
		array('name' => 'Телефон', 'price' => '300', 'quantity' => 3),
		array('name' => 'Кроссовки', 'price' => '150', 'quantity' => 2),
	);

	function getInfo($array){
	    $sum = 0;
	    $count = 0;
        foreach ($array as $value) {
            $sum += $value['price'] * $value['quantity'];
            $count += $value['quantity'];
        }
        return ['Total price' => $sum, 'Total quanity' => $count];
	}

    echo "<pre>";
	var_dump(getInfo($products));
    echo "</pre>";

    //---------------1.2---------------
    echo "<h3>Task 1.2</h3>";

	$a = rand(1, 5);
	$b = rand(1, 10);
	$c = rand(1, 5);
	
	function equation($a, $b, $c){
		$D = pow($b, 2) - 4 * $a * $c;
    	if ($D > 0) {
        	$x1 = ($b * (-1) + sqrt($D)) / (2 * $a);
        	$x2 = ($b * (-1) - sqrt($D)) / (2 * $a);
        	return ['x1' => $x1, 'x2' => $x2];
    	}else if ($D == 0) {
        	$x = ($b * (-1)) / 2 * $a;
        	return $x;
    	}else{
        	return false;
    	}
	}

	echo "<pre>";
	var_dump(equation($a, $b, $c));
    echo "</pre>";


    //---------------1.3---------------
    echo "<h3>Task 1.3</h3>";

    $digits = array(2,­10, ­2, 4, 5, 1, 6, 200, 1.6, 95);

    function deleteNegatives($array){
        foreach ($array as $value) {
            if ($value < 0) {
                $value = -$value;
            }
        }
        return $array;
    }

    $digits = deleteNegatives($digits);
    echo "<pre>";
    var_dump($digits);
    echo "</pre>";


    //---------------1.4---------------
    echo "<h3>Task 1.4</h3>";

    $digits2 = array(2,­10, ­2, 4, 5, 1, 6, 200, 1.6, 95);

    function deleteNegatives2(&$array){
        foreach ($array as $value) {
            if ($value < 0) {
                $value = -$value;
            }
        }
    }

    deleteNegatives2($digits2);
    echo "<pre>";
    var_dump($digits2);
    echo "</pre>";

?>