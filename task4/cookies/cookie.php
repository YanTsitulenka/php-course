<?php

$helloNewGuy = "Добро пожаловать, новичек!";
$helloOldGuy = "С возвращением, дружище!";

setcookie("hello", $helloOldGuy, time() + 36000);
setcookie("count", $_COOKIE["count"]+1);
setcookie("lastVisit", date("Y-m-d H:i:s"));

echo "<h3>Task 1</h3>";
if (!$_COOKIE["hello"]) {
    echo $helloNewGuy;
} else {
    echo $_COOKIE["hello"];
}


echo "<h3>Task 2</h3>";

if (!$_COOKIE["lastVisit"]) {
    echo "Это ваш первый визит";
} else {
    echo "Последний визит: " .  $_COOKIE["lastVisit"];
}


echo "<h3>Task 3</h3>";

if (!$_COOKIE["count"]) {
    echo "Количество посещений: 0";
} else {
    echo "Количество посещений: " .  $_COOKIE["count"];
}
