<?php

$dbConnection = mysqli_connect('localhost', 'root', '', 'news_site') or die("Ошибка " . mysqli_error($dbConnection));

$query = 'SELECT * FROM news';
$result = mysqli_query($dbConnection, $query);
while ($row = mysqli_fetch_assoc($result)) {
    echo '<h1>' . $row['header'] . '</h1>';
    echo '<p>' . mb_strimwidth($row['text'], 0, 100, '...') . '</p>';
    echo '<a href="/news/view/'. $row['id'] .'">Читать далее</a>';
}
mysqli_close($dbConnection);
