-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Фев 09 2017 г., 20:16
-- Версия сервера: 5.5.53
-- Версия PHP: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `news_site`
--

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `header` text NOT NULL,
  `text` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `category_id`, `user_id`, `header`, `text`, `status`) VALUES
(1, 1, 1, 'Фуркад отказался пожимать руку Логинову и ушел с церемонии награждения', 'Французский биатлонист Мартен Фуркад демонстративно ушел с пьедестала почета во время награждения сборной России бронзовыми медалями после смешанной эстафеты чемпионата мира в Хохфильцене. \r\nВидео было опубликовано на странице Международного союза биатлонистов (IBU) в Twitter.', 0),
(2, 1, 1, 'Генсек федерации биатлона Казахстана объяснил ситуацию с допингом', 'Коробки с отходами медикаментов, обнаруженные на одной из заправок города Инцель (Германия), стали причиной обысков в сборной Казахстана по биатлону. Об этом сообщил генеральный секретарь казахстанской федерации биатлона Манас Уссенов. Слова функционера приводит «Чемпионат.com».\r\n\r\n«23 января на одной из заправок города Инцель была обнаружена коробка с отходами медикаментов и аккредитацией врача нашей команды. Это и стало причиной вчерашнего обыска в нашей команде. Полиция явилась в отель, где проживает наша команда, и произвела обыск в комнатах», — сказал Уссенов.', 1),
(3, 2, 2, 'Дело Лапшина: Белоруссию заподозрили в попытке развала ЕврАзЕэ и ОДКБ', 'Cкандал вокруг российско-израильского блогера Александра Лапшина, переданного Белоруссией Азербайджану, поставил под вопрос будущее ОДКБ, Союзного государства и Евразийского союза. Эксперты считают, что своими действиями Минск топит эти организации. В среду вечером в Ереване протестующие окружили белорусское посольство, требуя высылки посла — в знак протеста против того, что Минск выдал третьей стране человека лишь за то, что он съездил в Нагорный Карабах.', 0),
(4, 2, 2, 'Путин выразил соболезнования Эрдогану после гибели турецких военнослужащих', 'Пресс-секретарь президента России Дмитрий Песков сообщил, что Владимир Путин выразил соболезнования президенту Турции Эрдогану в связи с трагическим инцидентом, повлекшим гибель нескольких турецких военнослужащих в районе города Эль-Баб. Ранее сообщалось, что ВКС РФ, которые наносили там удары по позициям террористов, случайно поразили здание, где находились турецкие военнослужащие. Всего погибло трое человек.', 1),
(5, 3, 3, 'Деноминация, обмен редких валют и покупка бриллиантов: о чем спрашивали у Нацбанка чаще всего\r\nЧитать полностью:  https://finance.tut.by/news530944.html', 'В 2016 году в контакт-центр Нацбанка белорусы позвонили 20 306 раз. Чаще всего, как и в 2015 году, они спрашивали о валюте. Но теперь людей больше интересовал не курс евро и доллара, а обмен редких видов валют, монет и поврежденных банкнот.\r\nЧитать полностью:  https://finance.tut.by/news530944.html', 0),
(6, 3, 3, 'В январе цены в Беларуси выросли на 0,9%\r\n\r\n', 'Январская инфляция в Беларуси составила 0,9%, сообщает Белстат. Лидерами по росту цен стали продовольственные товары, которые подорожали на 1,2%. Для сравнения: в январе прошлого года инфляция была на уровне 1,9%.', 1),
(7, 4, 4, 'Доказано существование невозможного типа черных дыр', 'Американские и австралийские астрофизики обнаружили кандидата в черные дыры средней массы. Такое название они получили потому, что тяжелее обычных — то есть формирующихся в результате гравитационного коллапса звезд — объектов, но легче сверхмассивных черных дыр, как правило расположенных в активных ядрах крупных галактик. Происхождение необычных объектов до сих пор остается неясным.', 0),
(8, 4, 4, 'SpaceX пообещала запускать ракеты дважды в месяц', 'Президент SpaceX Гвинн Шотвелл рассказала о намерении компании запускать ракеты каждые две-три недели. Об этом сообщает Newsmax.\r\n\r\nПредполагается, что план будет соблюден после введения в эксплуатацию новой стартовой площадки, которая находится в принадлежащем НАСА космическом центре Кеннеди, расположенном к югу от мыса Канаверал (штат Флорида). Это будет сделано уже на следующей неделе. Ее строительство обошлось примерно в 100 миллионов долларов.', 1),
(9, 5, 5, 'Трамп назвал Маккейна лузером', 'Президент США Дональд Трамп опубликовал несколько записей в своем Twitter, обрушившись с критикой на сенатора Джона Маккейна.\r\n«Сенатору Маккейну не следовало бы обсуждать в СМИ успех или провал операции. Это лишь вдохновляет врага. Он проигрывает так долго, что разучился побеждать. Просто посмотрите на этот бардак, наша страна увязла в конфликтах по всему миру», — написал Трамп', 0),
(10, 5, 5, 'Палестинец открыл огонь на рынке израильского города Петах-Тиква', 'Четыре человека получили ранения в результате нападения палестинца на посетителей рынка в городе Петах-Тиква, недалеко от Тель-Авива. Злоумышленник задержан. Видео публикует The Jerusalem Post. Трое пострадавших с огнестрельными ранениями и один с ножевыми госпитализированы. Власти не исключают версию теракта. В настоящее время подозреваемого допрашивают..', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `news_category`
--

CREATE TABLE `news_category` (
  `id` int(11) NOT NULL,
  `category` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news_category`
--

INSERT INTO `news_category` (`id`, `category`) VALUES
(1, 'спорт'),
(2, 'политика'),
(3, 'финансы'),
(4, 'наука и техника'),
(5, 'мир');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `login` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `surname` varchar(30) NOT NULL,
  `last_visit` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `name`, `surname`, `last_visit`) VALUES
(1, 'tomas123', '123', 'томас', 'смит', ''),
(2, 'tomas1234', '1234', 'томас', 'джон', ''),
(3, 'тигрица', '1', 'юлия', 'иванова', ''),
(4, 'superShadow007', '123456', 'иван', 'ковальчук', ''),
(5, 'loooogin', 'paaaaassword', 'боб', 'бобовский', '');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `news_category`
--
ALTER TABLE `news_category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `news_category`
--
ALTER TABLE `news_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
