<?php

class Figure
{
    protected $area;
    protected $color;
    const SIDES_COUNT = 0;

    public function infoAbout()
    {
        return 'Это геометрическая фигура!';
    }
}

class Rectangle extends Figure
{
    private $a;
    private $b;
    const SIDES_COUNT = 4;

    public function __construct($a, $b)
    {
        $this->a = $a;
        $this->b = $b;
    }

    public function getArea()
    {
        return $this->a * $this->b;
    }

    public final function infoAbout()
    {
        return 'Это класс прямоугольника, у него ' . self::SIDES_COUNT . 'стороны';
    }
}

class Triangle extends Figure
{
    private $a;
    private $b;
    private $c;
    const SIDES_COUNT = 3;

    public function __construct($a, $b, $c)
    {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
    }

    public function getArea()
    {
        $p = ($this->a + $this->b + $this->c) / 2;
        return sqrt($p * ($p - $this->a ) * ($p - $this->b) * ($p - $this->c));
    }

    public final function infoAbout()
    {
        return 'Это класс треугольника, у него ' . self::SIDES_COUNT . 'стороны';
    }
}

class Square extends Figure
{
    private $a;
    const SIDES_COUNT = 4;

    public function __construct($a)
    {
        $this->a = $a;
    }

    public function getArea()
    {
        return $this->a * $this->a;
    }

    public final function infoAbout()
    {
        return 'Это класс квадрата, у него ' . self::SIDES_COUNT . 'стороны';
    }
}

$array = [new Rectangle(1,2), new Rectangle(3,4), new Triangle(5,5,5), new Triangle(2,2,2), new Square(1), new Square(2)];

for($i = 0; $i < count($array); $i++) {
    echo $array[$i]->getArea() . '<br/>';
}
