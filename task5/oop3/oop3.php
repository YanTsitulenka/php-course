<?php

class Book
{
    const IMAGE = 'Book.png';
    public $bookName;
    public $fileName;

    public function __construct($bookName, $fileName)
    {
        $this->bookName = $bookName;
        $this->fileName = $fileName;
    }
}


class BookPdf extends Book
{
    const IMAGE = 'BookPdf.png';
}


class BookTxt extends Book
{
    const IMAGE = 'BookTxt.png';
}


class BookDoc extends Book
{
    const IMAGE = 'BookDoc.png';
}


function getBooksFromDB()
{
    $booksArray = [];
    $dbConnection = mysqli_connect('localhost', 'root', '', 'books') or die("Ошибка " . mysqli_error($dbConnection));
    $query = 'SELECT * FROM book';
    $result = mysqli_query($dbConnection, $query);

    while ($row = mysqli_fetch_assoc($result)) {
        $fileExtension = strtolower(substr($row['file_name'], -3));
        if ($fileExtension == 'doc') {
            array_push($booksArray, new BookDoc($row['name'], $row['file_name']));
        } else if ($fileExtension == 'pdf') {
            array_push($booksArray, new BookPdf($row['name'], $row['file_name']));
        } else if ($fileExtension == 'txt') {
            array_push($booksArray, new BookTxt($row['name'], $row['file_name']));
        } else {
            array_push($booksArray, new Book($row['name'], $row['file_name']));
        }
    }

    mysqli_close($dbConnection);
    return $booksArray;
}


$booksArray = getBooksFromDB();

foreach ($booksArray as $value) {
    echo '<p>';
    echo '<img src="images/' . $value::IMAGE . '" alt="" width="32" height="32" style="vertical-align:middle"/>';
    echo '<a href="/files/' . $value->fileName .  '" >' . $value->bookName . '</a>';
    echo '</p>';
}
