<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    public function genres(){
        return $this->belongsToMany('App\Genre', 'artists_genres', 'id_artist', 'id_genre');
    }

    public function songs(){
        return $this->belongsToMany('App\Song', 'artists_songs', 'id_artist', 'id_song');
    }
}