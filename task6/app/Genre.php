<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    public function artists()
    {
        return $this->belongsToMany('App\Artist', 'artists_genres', 'id_genre', 'id_artist');
    }
}