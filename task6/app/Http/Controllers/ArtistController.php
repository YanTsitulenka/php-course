<?php

namespace App\Http\Controllers;

use App\Artist;

class ArtistController extends Controller
{
    public function show($artistAlias)
    {
        $artist = Artist::where('alias', $artistAlias)->first();

        return view('artist')->with(['artist' => $artist]);
    }


}