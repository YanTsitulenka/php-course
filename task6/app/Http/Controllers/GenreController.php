<?php

namespace App\Http\Controllers;


use App\Genre;

class GenreController extends Controller
{
    public function show($genreAlias)
    {

        $genre = Genre::where('alias', $genreAlias)->first();
        $artists = $genre->artists()->paginate(3);

        return view('index')->with(['artists' => $artists]);
    }
}