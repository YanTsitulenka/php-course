<?php

namespace App\Http\Controllers;

use App\Artist;

class IndexController extends Controller
{
    public function show()
    {
        $artists = Artist::paginate(3);
        return view('index')->with(['artists' => $artists]);
    }
}