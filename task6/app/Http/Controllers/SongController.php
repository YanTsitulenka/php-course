<?php

namespace App\Http\Controllers;

use App\Artist;
use App\Song;
class SongController
{
    public function show($artistAlias, $songAlias)
    {
        $artist = Artist::select()->where('alias', $artistAlias)->first();
        $song = Song::select()->where('alias', $songAlias)->first();

        return view('song')->with(['song' => $song, 'artist' => $artist]);
    }
}