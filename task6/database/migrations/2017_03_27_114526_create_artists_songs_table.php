<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistsSongsTable extends Migration
{
    public function up()
    {
        Schema::create('artists_songs', function (Blueprint $table) {
            $table->integer('id_artist');
            $table->integer('id_song');
        });
    }

    public function down()
    {
        Schema::drop('artists_songs');
    }
}
