<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistsGenresTable extends Migration
{
    public function up()
    {
        Schema::create('artists_genres', function (Blueprint $table) {
            $table->integer('id_artist');
            $table->integer('id_genre');
        });
    }

    public function down()
    {
        Schema::drop('artists_genres');
    }
}
