<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        DB::table('artists')->insert([
            [
                'id' => NULL,
                'name' => 'Ляпис Трубецкой',
                'image' => 'https://avatars.yandex.net/get-music-content/49876/599ae314.p.41166/m1000x1000',
                'alias' => 'lyapis-trubetskoy',
                'biography' => '«Ля́пис Трубецко́й» — белорусская панк-рок-группа, названная в честь комического героя романа Ильи Ильфа и Евгения Петрова «Двенадцать стульев», поэта-халтурщика Никифора Ляписа, который печатался под псевдонимом Трубецкой.
                        Лауреат премий RAMP, «Рок-коронация», «Чартова дюжина», «Степной волк», ZD Awards, Ultra-Music Awards.
                        17 марта 2014 года Сергей Михалок объявил о роспуске группы, и 31 августа группа прекратила свою деятельность[2] и распалась на два коллектива: «Brutto» и «Trubetskoy».'
            ],[
                'id' => NULL,
                'name' => 'Metallica',
                'image' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Metallica_at_The_O2_Arena_London_2008.jpg/1280px-Metallica_at_The_O2_Arena_London_2008.jpg',
                'alias' => 'metallica',
                'biography' => 'Metallica (читается как Метáллика) — американская метал-группа[, образованная в 1981 году.
                Metallica оказала большое влияние на развитие метала и входит (наряду с такими группами как Slayer, Megadeth и Anthrax) в «большую четвёрку трэш-метала». 
                Альбомы Metallica были проданы в общей сложности в количестве более 100 миллионов экземпляров по всему миру, что делает её одним из самых коммерчески успешных металлических коллективов. 
                В 2011 году один из крупнейших журналов о метал-музыке Kerrang! в июньском номере признал Metallica лучшей метал-группой последних 30 лет.'
            ],[
                'id' => NULL,
                'name' => 'System Of A Down',
                'image' => 'https://avatars.yandex.net/get-music-content/38044/ada8f733.p.67437/m1000x1000',
                'alias' => 'system-of-a-down',
                'biography' => 'System of a Down (SOAD) — американская рок-группа, образованная в 1992 году в Лос-Анджелесе Сержем Танкяном и Дароном Малакяном под названием Soil, а в 1995 принявшая нынешнее название. Все участники группы имеют армянское происхождение. 
                В период с 1998 по 2005 год группа выпустила пять студийных альбомов, каждый из которых стал платиновым (наиболее успешный — мультиплатиновый Toxicity, с общим тиражом свыше 12 миллионов экземпляров). В 2006 году участники System of a Down решили временно приостановить совместную деятельность и заняться сольными проектами. 
                29 ноября 2010 года группа объявила о воссоединении и проведении европейского турне в 2011 году. Изначально группа должна была называться «Victims of the Down» — по стихотворению, написанному Дароном Малакяном. При совместном обсуждении участниками было решено заменить слово «victims» на более общее «system». 
                Причиной замены также послужило желание Шаво Одаджяна расположить группу ближе к Slayer на полках музыкальных магазинов.'
            ],[
                'id' => NULL,
                'name' => 'Земфира',
                'image' => 'https://24smi.org/public/media/2016/10/15/44ca0faa4fe9430aec710f076b0cff7d.jpg',
                'alias' => 'zemfira',
                'biography' => 'Земфира (полное имя Земфи́ра Талга́товна Рамаза́нова (тат. Земфира Тәлгать кызы Рамазанова); род. 26 августа 1976, Уфа, Башкирская АССР, РСФСР, СССР) — российская рок-певица, музыкант, композитор, продюсер и автор песен. Земфира стала олицетворением нового движения в русском роке, которое журналисты окрестили «женский рок».'
            ]
        ]);


        DB::table('genres')->insert([
            [
                'id' => NULL,
                'name' => 'Русский рок',
                'alias' => 'russian-rock'
            ],[
                'id' => NULL,
                'name' => 'Поп-рок',
                'alias' => 'pop-rock'
            ],[
                'id' => NULL,
                'name' => 'Инди-рок',
                'alias' => 'indie-rock'
            ],[
                'id' => NULL,
                'name' => 'Хеви-метал',
                'alias' => 'heavy-metal'
            ],[
                'id' => NULL,
                'name' => 'Хард-рок',
                'alias' => 'hard-rock'
            ],[
                'id' => NULL,
                'name' => 'Трэш-метал',
                'alias' => 'trash-metal'
            ],[
                'id' => NULL,
                'name' => 'Альтернативный рок',
                'alias' => 'alternative-rock'
            ],[
                'id' => NULL,
                'name' => 'Фолк-рок',
                'alias' => 'folk-rock'
            ],[
                'id' => NULL,
                'name' => 'Ска-панк',
                'alias' => 'ska-punk'
            ],[
                'id' => NULL,
                'name' => 'Альтернативный метал',
                'alias' => 'alternative-metal'
            ],[
                'id' => NULL,
                'name' => 'Ню-метал',
                'alias' => 'new-metal'
            ],[
                'id' => NULL,
                'name' => 'Хард-рок',
                'alias' => 'hard-rock'
            ],[
                'id' => NULL,
                'name' => 'Фанк-метал',
                'alias' => 'funk-metal'
            ]
        ]);



        DB::table('songs')->insert([
            [
                'id' => NULL,
                'name' => 'Воины света',
                'alias' => 'voini-sveta',
                'text' => 'Рубиновые части, солнца зари
Рубят злые страсти, сжигают внутри.
Прыгай выше неба, брат и сестра;
Золотые искры — брызги костра.

Радуйся молоту в крепкой руке!
Водопад, молодость — в быстрой реке.
Бей барабан — бам, бам!
Баррикады, друзья, шум, гам.

Воины света, воины добра
Охраняют лето, бьются до утра.
Воины добра! Воины света!
Джа Растафарай* бьются до рассвета.

Плачет солдат, медаль на гимнастёрке.
Сколько ребят в полыни на пригорке.
За тучей — дракон, каменное сердце.
Ночью — закон, руби, чтобы согреться.

Радуйся молоту в крепкой руке!
Водопад, молодость — в быстрой реке.
Бей барабан — бам, бам!
Баррикады, друзья, шум, гам.

Воины света, воины добра
Охраняют лето, бьются до утра.
Воины добра! Воины света!
Джа Растафарай бьются до рассвета.

Воины света, воины добра
Охраняют лето, бьются до утра.
Воины добра! Воины света!
Джа Растафарай охраняют лето.

Воины света, воины добра
Охраняют лето, бьются до утра.
Воины добра! Воины света!
Джа Растафарай бьются до рассвета!'
            ],[
                'id' => NULL,
                'name' => 'Я верю',
                'alias' => 'ya-veru',
                'text' => 'Адам и Ева проснулись с утра,
Их друг Гелиос гордился работой,
Всю неделю будет жара,
Дельфийский оракул следил за погодой.

Карлик небесный — крылатый колибри —
Любил на заре напиться росою,
Зебры, кентавры, волки и тигры,
Мифы бурлят горной рекою.

Я верю в Иисуса Христа, я верю в Гаутаму Будду,
Я верю в пророка Мухаммеда, я верю в Кришну, я верю в Гаруду.
Я верю в Иисуса Христа, верю в Гаутаму Будду,
Я верю Джа, я верю Джа, я верю Джа и верить буду!

Хромой Вулкан разводит огонь —
Тору нужен кованый молот,
Единый Творец держит ладонь
Над картой жизни, где будет город.

Легенды делают нас мудрей,
Мы чувствуем пульс единой Вселенной!
Апостол Пётр — хранитель ключей —
В волшебном танце с Прекрасной Еленой.

Я верю в Иисуса Христа, я верю в Гаутаму Будду,
Я верю в пророка Мухаммеда, я верю в Кришну, я верю в Гаруду.
Я верю в Иисуса Христа, верю в Гаутаму Будду,
Я верю Джа, я верю Джа, я верю Джа и верить буду!

Я верю в Иисуса Христа, я верю в Гаутаму Будду,
Я верю в пророка Мухаммеда, я верю в Кришну, я верю в Гаруду.
Я верю в Иисуса Христа, я верю в Гаутаму Будду,
Я верю Джа, я верю в Любовь, я верю в Добро и верить буду!

И верить буду!
И верить буду!
И верить буду!'
            ],[
                'id' => NULL,
                'name' => 'В платье белом',
                'alias' => 'v-platie-belom',
                'text' => 'Когда зимой холодною в крещенские морозы
Щебечет песню соловей и распускаются мимозы
Когда взлетаешь к небесам и там паришь, пугая звезды
А над окном твоим совьют какие-нибудь птицы гнезда
Когда девчонка толстая журнал приобретает "Мода"
И снит, как будто юноши ей в школе не дают прохода
Когда милиционер усатый вдруг улыбнется хулигану
И поведет его под руки, но не в тюрьму, а к ресторану

Знай, это любовь
С ней рядом Амур крыльями машет
Знай, это любовь
Сердце не прячь - Амур не промажет

Когда мальчишка на асфальте мелом пишет чье-то имя
Когда теленок несмышленый губами ищет мамки вымя
Когда веселый бригадир доярку щиплет возле клуба
когда солдатик лысенький во сне целует друга губы
когда безродная дворняга забраться хочет на бульдога
Когда в купаловскую ночь две пары ног торчат из стога
Когда седой профессор под дождем по лужам резво скачет
А зацелованная им девчонка над пятеркой плачет

Знай, это любовь
С ней рядом Амур крыльями машет
Знай, это любовь
Сердце не прячь - Амур не промажет

Любовь зимой приходит в платье белом
Весной любовь приходит в платье голубом
Любовь приходит летом в платьице зеленом
А осенью любовь приходит в золотом

Любовь зимой всегда приходит в платье белом
Весной любовь приходит в платье голубом
Любовь приходит летом в платьице зеленом
А осенью любовь приходит в золотом

Знай, это любовь
С ней рядом Амур крыльями машет
Знай, это любовь
Сердце не прячь - Амур не промажет

Любовь зимой приходит в платье белом
Весной любовь приходит в платье голубом
Любовь приходит летом в платьице зеленом
А осенью любовь приходит в золотом

Любовь зимой всегда приходит в платье белом
Весной любовь приходит в платье голубом 
Любовь приходит летом в платьице зеленом
А осенью любовь приходит в золотом 
А осенью любовь приходит в золотом'
            ],[
                'id' => NULL,
                'name' => 'Enter Sandman',
                'alias' => 'enter-sandman',
                'text' => 'Say your prayers little one
Don’t forget, my son
To include everyone
Tuck you in, warm within
Keep you free from sin
Till the sandman he comes
Sleep with one eye open
Gripping your pillow tight
Exit light
Enter night
Take my hand
Off to never never land
Something’s wrong, shut the light
Heavy thoughts tonight
And they aren’t of snow white
Dreams of war, dreams of liars
Dreams of dragon’s fire
And of things that will bite
Sleep with one eye open
Gripping your pillow tight
Exit light
Enter night
Take my hand
Off to never never land
Now I lay me down to sleep
Pray the lord my soul to keep
If I die before I wake
pray the lord my soul to take
Hush little baby, don’t say a word
And never mind that noise you heard
It’s just the beast under your bed,
In your closet, in your head
Exit light
Enter night
Grain of sand
Exit light
Enter night
Take my hand
We’re off to never never land'
            ],[
                'id' => NULL,
                'name' => 'Nothing Else Matters',
                'alias' => 'nothing-else-matters',
                'text' => 'So close no matter how far
couldn’t be much more from the heart
forever trusting who we are
and nothing else matters
never opened myself this way
life is ours, we live it our way
all these words I don’t just say
and nothing else matters
trust I seek and I find in you
every day for us something new
open mind for a different view
and nothing else matters
never cared for what they do never cared for what they know
but I know
so close no matter how far
couldn’t be much more from the heart
forever trusting who we are
and nothing else matters
never cared for what they do never cared for what they know
but I know
never opened myself this way
life is ours, we live it our way
all these words I don’t just say
and nothing else matters
trust I seek and I find in you
every day for us something new
open mind for a different view
and nothing else matters
never cared for what they say
never cared for games they play
never cared for what they do never cared for what they know
and I know
so close no matter how far
couldn’t be much more from the heart
forever trusting who we are
no nothing else matters'
            ],[
                'id' => NULL,
                'name' => 'Atlas, Rise!',
                'alias' => 'atlas-rise',
                'text' => 'Bitterness and burden
Curses rest on thee
Solitaire and sorrow
All eternity
Save the Earth and claim perfection
Deem the mass and blame rejection
Hold the pose, fein perception
Grudges break your back
All you bear
All you carry
All you bear
Place it right on Right on me Die as you suffer in vain
Own all the grief and the pain
Die as you hold up the skies
Atlas, rise!
How does it feel on your own?
Bound by the world all alone
Crushed under heavy skies
Atlas, rise
Crucify and witness
Circling the sun
Bastardized in ruin
What have you become?
Blame the world and blame your maker
Wish \'em to the undertaker
Crown yourself the other savior
So you carry on All you bear
All you carry
All you bear
Place it right on Right on me Die as you suffer in vain
Own all the grief and the pain
Die as you hold up the skies
Atlas, rise!
How does it feel on your own?
Bound by the world all alone
Crushed under heavy skies
Crushed under heavy skies
Atlas, rise!
Masquerade as maker
Heavy is the crown
Beaten down and broken
Drama wears you down
Overload, the martyr stumbles
Hit the ground and heaven crumbles
All alone the fear shall humble
Swallow all your pride
All you bear
All you carry
All you bear
Place it right on Right on me Die as you suffer in vain
Own all the grief and the pain
Die as you hold up the skies
Atlas, rise
How does it feel on your own?
Bound by the world all alone
Crushed under heavy skies
Crushed under heavy skies
Atlas, rise'
            ],[
                'id' => NULL,
                'name' => 'Chop Suey!',
                'alias' => 'chop-suey',
                'text' => 'Wake up 
Grab a brush and put on a little makeup 
Hide the scars to fade away the shakeup 
Why\'d you leave the keys upon the table? 
Here you go, create another fable 

You wanted to 
Grab a brush and put a little makeup 
You wanted to 
Hide the scars to fade away the shakeup 
You wanted to 
Why\'d you leave the keys upon the table? 
You wanted to 

I don\'t think you trust 
In my self-righteous suicide 
I cry when angels deserve to die 

Wake up 
Grab a brush and put on a little makeup 
Hide the scars to fade away the (Hide the scars to fade away the) 
Why\'d you leave the keys upon the table? 

Here you go, create another fable 

You wanted to 
Grab a brush and put a little makeup 
You wanted to 
Hide the scars to fade away the shakeup 
You wanted to 
Why\'d you leave the keys upon the table? 
You wanted to 

I don\'t think you trust 
In my self-righteous suicide 
I cry when angels deserve to die 
In my self-righteous suicide 
I cry when angels deserve to die 

Father! (Father!)
Father! (Father!)
Father! (Father!)
Father! (Father!)
Father, into your hands I commit my spirit 
Father, into your hands 

Why have you forsaken me? 
In your eyes forsaken me 
In your thoughts forsaken me 
In your heart forsaken me, oh 

Trust in my self-righteous suicide 
I cry when angels deserve to die 
In my self-righteous suicide 
I cry when angels deserve to die'
            ],[
                'id' => NULL,
                'name' => 'Toxicity',
                'alias' => 'toxicity',
                'text' => 'Conversion software version 7.0
Looking at life through the eyes of a tire hub
Eating seeds as a pastime activity
The toxicity of our city, of our city
New. What do you own the world?
How do you own disorder, disorder
Now. Somewhere between the sacred silence
sacred silence and sleep
Somewhere between the sacred silence and sleep
Disorder, disorder, disorder
More wood for their fires, loud neighbours
Flashlight reveries caught in the headlights of a truck
Eating seeds as a pastime activity
The toxicity of our city, of our city
New. What do you own the world?
How do you own disorder, disorder
Now, somewhere between the sacred silence
Sacred silence and sleep
Somewhere between the sacred silence and sleep
Disorder, disorder, disorder
New. What do you own the world?
How do you own disorder, disorder
Now. Somewhere between the sacred silence and sleep
Sacred silence and sleep
Somewhere between the sacred silence and sleep
Disorder, disorder, disorder
When I became the sun
I shone life
into the man’s hearts
When I became the sun
I shone life into the man’s hearts'
            ],[
                'id' => NULL,
                'name' => 'Aerials',
                'alias' => 'aerials',
                'text' => 'Life is a waterfall
we’re one in the river
and one again after the fall
swimming through the void
we hear the word
we lose ourselves
but we find it all…
cause we are the ones that want to play
always want to go but you never want to stay
and we are the ones that want to choose
always want to play
but you never want to lose
aerials, in the sky
when you lose small mind
you free your life
life is a waterfall
we drink from the river
then we turn around and put up our walls
swimming through the void
we hear the word
we lose ourselves
but we find it all…
cause we are the ones that want to play
always want to go but you never want to stay
and we are the ones that want to choose
always want to play
but you never want to lose
aerials, in the sky
when you lose small mind
you free your life
aerials, so up high
when you free your eyes eternal prize
aerials, in the sky
when you lose small mind
you free your life
aerials, so up high
when you free your eyes eternal prize'
            ],[
                'id' => NULL,
                'name' => 'Lonely Day',
                'alias' => 'lonely-day',
                'text' => 'Such a lonely day
And its mine
The most loneliest day of my life
Such a lonely day
Should be banned
It’s a day that I can’t stand
The most loneliest day of my life
The most loneliest day of my life
Such a lonely day
Shouldn’t exist
It’s a day that I’ll never miss
Such a lonely day
And its mine
The most loneliest day of my life
And if you go, I wanna go with you
And if you die, I wanna die with you
Take your hand and walk away
The most loneliest day of my life
The most loneliest day of my life
The most loneliest day of my life
Life
Such a lonely day
And its mine
It’s a day that I’m glad I survived'
            ],[
                'id' => NULL,
                'name' => 'Искала',
                'alias' => 'iskala',
                'text' => 'Я искала тебя
Годами долгими
Искала тебя
Дворами темными
В журналах
в кино
Среди друзей
В день
когда нашла
С ума сошла

Ты
совсем как во сне
Совсем как в альбомах
Где я рисовала тебя гуашью

Дальше были звонки
Ночные
Больше
Слезы
нервы
любовь
И стрелки в Польше
Дети
но не мои
Старые зазнобы
Куришь
каждые пять
Мы устали оба

Ты
совсем как во сне
Совсем как в альбомах
Где я рисовала тебя гуашью

Годами долгими
Ночами темными
Годами долгими

Ты
совсем как во сне
Совсем как в альбомах
Где я рисовала тебя гуашью
Ты
совсем как во сне
Совсем как в альбомах
Где я рисовала тебя гуашью

Я искала тебя
Годами долгими
Искала тебя
Дворами темными
В журналах
в кино
Годами долгими
Ночами, чами, чами, чами...'
            ],[
                'id' => NULL,
                'name' => 'Ариведерчи',
                'alias' => 'arivederchi',
                'text' => 'Вороны москвички меня разбудили
Промокшие спички надежду убили
Курить значит буду
Дольше жить значит будем
Корабли в моей гавани жечь
На рубли поменяю билет
Отрастить бы до самых бы плеч
Я никогда не вернусь домой
C тобой мне так интересно,
А с ними не очень
Я вижу что тесно я помню что прочно
Дарю время видишь
Я горю кто-то спутал
И поджег меня ариведерчи
Не учили в глазок посмотреть
И едва ли успеют по плечи
Я разобью турникет
И побегу по своим
Обратный «чэйндж» на билет
Я буду ждать ты звони
В мои обычные шесть
Я стала старше на жизнь
Наверное нужно учесть
Корабли в моей гавани
Не взлетим так поплаваем
Стрелки ровно на два часа назад
Корабли в моей гавани
Не взлетим так поплаваем
Стрелки ровно на два часа назад'
            ],[
                'id' => NULL,
                'name' => 'Хочешь?',
                'alias' => 'hochesh',
                'text' => 'Пожалуйста, не умирай
Или мне придется тоже.
Ты, конечно, сразу в рай,
А я не думаю, что тоже.
Хочешь сладких апельсинов?
Хочешь вслух рассказов длинных?
Хочешь я взорву все звезды,
Что мешают спать?

Пожалуйста, только живи!
Ты же видишь, я живу тобою.
Моей огромной любви
Хватит нам двоим с головою.
Хочешь море с парусами?
Хочешь музык новых самых?
Хочешь я убью соседей,
Что мешают спать?

Хочешь солнце вместо лампы?
Хочешь за окошком Альпы?
Хочешь я отдам все песни,
Про тебя отдам все песни?
Хочешь солнце вместо лампы?
Хочешь за окошком Альпы?
Хочешь я отдам все песни,
Про тебя отдам все песни...Я?'
            ]
        ]);



        DB::table('artists_genres')->insert([
            [
                'id_artist' => 1,
                'id_genre' => 1
            ],[
                'id_artist' => 1,
                'id_genre' => 7
            ],[
                'id_artist' => 1,
                'id_genre' => 8
            ],[
                'id_artist' => 1,
                'id_genre' => 9
            ],[
                'id_artist' => 2,
                'id_genre' => 7
            ],[
                'id_artist' => 2,
                'id_genre' => 10
            ],[
                'id_artist' => 2,
                'id_genre' => 11
            ],[
                'id_artist' => 2,
                'id_genre' => 12
            ],[
                'id_artist' => 2,
                'id_genre' => 13
            ],[
                'id_artist' => 3,
                'id_genre' => 4
            ],[
                'id_artist' => 3,
                'id_genre' => 5
            ],[
                'id_artist' => 3,
                'id_genre' => 6
            ],[
                'id_artist' => 4,
                'id_genre' => 1
            ],[
                'id_artist' => 4,
                'id_genre' => 2
            ],[
                'id_artist' => 4,
                'id_genre' => 3
            ]
        ]);

        DB::table('artists_songs')->insert([
            [
                'id_artist' => 1,
                'id_song' => 1
            ],[
                'id_artist' => 1,
                'id_song' => 2
            ],[
                'id_artist' => 1,
                'id_song' => 3
            ],[
                'id_artist' => 2,
                'id_song' => 4
            ],[
                'id_artist' => 2,
                'id_song' => 5
            ],[
                'id_artist' => 2,
                'id_song' => 6
            ],[
                'id_artist' => 3,
                'id_song' => 7
            ],[
                'id_artist' => 3,
                'id_song' => 8
            ],[
                'id_artist' => 3,
                'id_song' => 9
            ],[
                'id_artist' => 3,
                'id_song' => 10
            ],[
                'id_artist' => 4,
                'id_song' => 11
            ],[
                'id_artist' => 4,
                'id_song' => 12
            ],[
                'id_artist' => 4,
                'id_song' => 13
            ]
        ]);
    }
}
