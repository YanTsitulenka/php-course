@extends('layouts.site')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <img class="img-responsive" src="{{ $artist -> image}}" alt="">
            </div>
            <div class="col-md-5">
                <h2>{{ $artist -> name }}</h2>
                <p>{{ $artist -> biography }}</p>
                <h3>Жанры</h3>
                <div class="btn-group">
                    @foreach($artist->genres as $genre)
                        <a href="{{ route('showGenre', $genre -> alias) }}" type="button" class="btn btn-default">{{ $genre -> name }}</a>
                    @endforeach
                </div>
            </div>
            <div class="row">
                <div class="song-block">
                    <div class="col-md-12">
                        <h3>Песни</h3>
                        @foreach($artist->songs as $song)
                            <h4><a href="{{$artist -> alias}}/{{ $song -> alias }}">{{ $song -> name }}</a></h4>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection