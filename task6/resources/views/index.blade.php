@extends('layouts.site')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Исполнители</h1>
            </div>
        </div>

        <div class="row">
            @foreach( $artists as $artist )
                <div class="col-md-4 portfolio-item">
                    <a href="#">
                        <img class="img-responsive" src="{{ $artist -> image }}" alt="">
                    </a>
                    <h3>
                        <a href="{{ route('showArtist', $artist->alias) }}">{{ $artist -> name }}</a>
                    </h3>
                    <div>
                        <h4>Количество песен: {{ $artist -> songs()->count() }}</h4>
                    </div>
                    <div class="btn-group">
                        @foreach($artist->genres as $genre)
                            <a href="{{ route('showGenre', $genre -> alias) }}" type="button" class="btn btn-default">{{ $genre -> name }}</a>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>

        <div class="row text-center pagination-block">
            <hr>
            <div class="col-lg-12">
                {{ $artists -> links() }}
            </div>
        </div>
    </div>
@endsection