@extends('layouts.site')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h3><a href="{{ route('showArtist', $artist -> alias) }}">{{ $artist -> name }}</a> - {{ $song -> name }}</h3>
                <pre>{{ $song -> text }}</pre>
            </div>
        </div>
    </div>

@endsection