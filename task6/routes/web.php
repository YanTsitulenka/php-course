<?php


Route::get('/', 'IndexController@show')->name('index');
Route::get('/genres/{genreAlias}', 'GenreController@show')->name('showGenre');
Route::get('/{artistAlias}', 'ArtistController@show')->name('showArtist');
Route::get('/{artistAlias}/{songAlias}', 'SongController@show')->name('showSong');